﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throwable : GameItem {
	bool thrown = false;

	public virtual void Throw () {}

	public override string GeneralType() {
		Debug.Log ("Name: " + itemName);
		return itemName;
	}
}
