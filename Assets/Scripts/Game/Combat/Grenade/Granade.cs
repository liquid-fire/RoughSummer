﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (SphereCollider))]
public class Granade : Throwable {
	[SerializeField] Rigidbody rigidBody;
	[SerializeField] [Range (20f, 100f)] float damage;

	public List <HitPoint> inAreaOfEffect;
	bool exploded = false;

	void Start () {
		inAreaOfEffect = new List<HitPoint> ();
		itemName = "HE Nade";
		Throw ();
	}

	void explode () {
		if (!exploded) {
			Debug.Log ("Kaboom");
			GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).RaiseEvent ("GrenadeExploded");
			foreach (var item in inAreaOfEffect) {
				item.TakeDamage (damage);
			}
			Destroy (this.gameObject);
		}
	}

	void OnTriggerEnter(Collider other) {
		HitPoint hp = other.transform.GetComponent<HitPoint> ();
		if (hp != null) {
			inAreaOfEffect.Add (hp);
		}
	}

	void OnTriggerExit(Collider other) {
		HitPoint hp = other.transform.GetComponent<HitPoint> ();
		if (inAreaOfEffect.Contains (hp)) {
			inAreaOfEffect.Remove (hp);
		}
	}

	bool startMovement;
	Vector3[] points;
	int index = 0;
	float nextTime;

	[ContextMenu ("Throw")]
	public override void Throw () {
		// add force here.
		// points = GetComponent<ProjectileArcRenderer> ().points;
		// startMovement = true;
		Vector3 dir = Quaternion.AngleAxis (30f, transform.up) * transform.right;
		rigidBody.useGravity = true;
		rigidBody.AddForce (dir * 100);
		GameManager.Instance.GetComponent<Timer> (EPlugInRef.Timer).Add (explode, 1f);
	}

	public override string GeneralType() {
		Debug.Log ("Name: " + itemName);
		return "HE Grenade";
	}

	void Update () {
		// if (startMovement) {
		// 	if (Time.time > nextTime) {
		// 		nextTime = Time.time + 2000;

		// 		index++;
		// 		transform.position = Vector3.Lerp (transform.position, points[index], 4);
		// 	}
		// }
	}
}
