﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Weapon : GameItem {
	float heatLevel;
	float beginCooldownCounter;

	public enum WeaponType {
		Rifle,
		HandGun,
		ShotGun,
	}

	[Header("WeaponState")]
	public FiringState WeaponFiringState = FiringState.NotFiring;
	public ReadyState WeaponReadyState = ReadyState.Ready;
	public ReloadingState WeapsonReloadingState = ReloadingState.NotReloading;
	public OverheatState WeaponOverheatState = OverheatState.NotOverHeated;

	[Header ("Components")]
	[SerializeField] Transform muzzle;
	[SerializeField] GameObject bullet;

	[Header ("Weapon Configuration")]
	[SerializeField] WeaponType weaponType;
	public Ammo.AmmoType usesAmmo;
	public WeaponType Type {
		get {
			return weaponType;
		}
	}
	[SerializeField] [Range (100, 600)] float fireRate;
	[SerializeField] int magazineSize;
	//[SerializeField] int ammoPool;
	public Inventory Inventory {
		get {
			return GameManager.Instance.GetComponent<Inventory> (EPlugInRef.Inventory);
		}
	}

	public int ammoPool {
		
		get {
			Ammo.AmmoType ammoType = usesAmmo;
			// if (weaponType == WeaponType.Rifle) 
			// 	ammoType = Ammo.AmmoType.RifleAmmo;
			// else if (weaponType == WeaponType.HandGun)
			// 	ammoType = Ammo.AmmoType.HandGunAmmo;
			// else if (weaponType == WeaponType.ShotGun) 
			// 	ammoType = Ammo.AmmoType.ShotGunShell;

			return Inventory.Ammo.AmmoCount (ammoType);
		}

		set {
			Ammo.AmmoType ammoType = usesAmmo;
			// if (weaponType == WeaponType.Rifle) 
			// 	ammoType = Ammo.AmmoType.RifleAmmo;
			// else if (weaponType == WeaponType.HandGun)
			// 	ammoType = Ammo.AmmoType.HandGunAmmo;
			// else if (weaponType == WeaponType.ShotGun) 
			// 	ammoType = Ammo.AmmoType.ShotGunShell;
			
			Inventory.Ammo.SetAmmo (value, ammoType);
		}
	}
	[SerializeField] [Range (0.1f, 5)] float realoadTime;
	[SerializeField] [Range (15, 45)] float damage;

	[Header ("Weapon Heatup Config")]
	[SerializeField] [Range (.1f, 5)] float overheatIn;
	[SerializeField] [Range (.1f, .9f)] float heatPerFire;
	[SerializeField] [Range (1, 3)] float coolDownTime;
	[SerializeField] [Range (0, 3)] float beginCooldownIn = 0.4f;

	[Header ("UI")]
	[SerializeField] Slider OverheatBar;
	[SerializeField] Text Message;
	[SerializeField] Text AmmoCount;

	float nextFire;
	float effectiveFireRate;
	float bulletsInMagzine;
	bool canFire {
		get {
			if (WeaponReadyState == ReadyState.NotReady)
				return false;
			if (WeapsonReloadingState == ReloadingState.Reloading)
				return false;
			if (WeaponOverheatState == OverheatState.Overheated)
				return false;

			return true;
		}
	}
	bool overheated = false;
	bool beginCooldown = false;

	public enum FiringState {
		Firing,
		NotFiring,
	}

	public enum ReloadingState {
		Reloading,
		NotReloading,
	}

	public enum ReadyState {
		Ready,
		NotReady
	}

	public enum OverheatState {
		Overheated,
		NotOverHeated,
	}

	void Awake() {
		EventBus.EventListener shopClosedListenter = new EventBus.EventListener ();
		shopClosedListenter.IsSingleShot = false;
		shopClosedListenter.Method = (object obj) => {
			AmmoCount.text = bulletsInMagzine + "/" + ammoPool;
		};
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("ShopClosed", shopClosedListenter);
		AmmoCount.text = bulletsInMagzine + "/" + ammoPool;
	}

	void Start() {
		effectiveFireRate = 1 / (fireRate/60);
		OverheatBar.maxValue = overheatIn;
		bulletsInMagzine = magazineSize;
		AmmoCount.text = bulletsInMagzine + "/" + ammoPool;
		muzzle = GameObject.Find ("Muzzle").transform;
		GameManager.Instance.GetComponent<Timer> (EPlugInRef.Timer).Add (() => {
			if (beginCooldown && WeaponFiringState != FiringState.Firing && heatLevel != 0) {
				heatLevel -= .1f;
				if (heatLevel < 0)
					heatLevel = 0;
			}	
		}, .1f, "weapon_cooldown", true);
	}

	[ContextMenu ("Fire")]
	public void Fire () {
		Debug.Log ("Ammo is: " + ammoPool);
		if (canFire) {
			WeaponFiringState = FiringState.Firing;
			bulletsInMagzine --;
			AmmoCount.text = bulletsInMagzine + "/" + ammoPool;
			GameObject round = Instantiate (bullet, muzzle.position, Quaternion.identity);
			round.GetComponent<Bullet> ().Damage = damage;
			WeaponReadyState = ReadyState.NotReady;
			nextFire = Time.time + effectiveFireRate;
			heatUp ();			
			beginCooldownCounter = 0;
			beginCooldown = false;
			if (bulletsInMagzine <= 0) {
				WeapsonReloadingState = ReloadingState.Reloading;
				reload ();
			}
		}
	}

	public void NotFire () {

	}

	void heatUp () {
		heatLevel += heatPerFire;
		if (heatLevel >= overheatIn) {
			Debug.Log ("Gun over heated");
			WeaponOverheatState = OverheatState.Overheated;
			Message.text = "Overheated";
			GameManager.Instance.GetComponent<Timer> (EPlugInRef.Timer).Add (() => {
				WeaponOverheatState = OverheatState.NotOverHeated;
				heatLevel = 0;
				Debug.Log ("Gun cooled down");
				Message.text = "";
			}, coolDownTime);
		}
	}

	void reload () {
		WeapsonReloadingState = ReloadingState.Reloading;
		if (ammoPool <= 0) {
			Message.text = "Empty";
			return;
		}
		Message.text = "Reloading";
		GameManager.Instance.GetComponent<Timer> (EPlugInRef.Timer).Add (() => {
			WeapsonReloadingState = ReloadingState.NotReloading;
			Debug.Log ("Gun reloaded");
			Message.text = "";
			WeaponFiringState = FiringState.NotFiring;
			if (ammoPool < magazineSize) {
				bulletsInMagzine = ammoPool;
				ammoPool = 0;
			} else {
				bulletsInMagzine = magazineSize;
				ammoPool = ammoPool - magazineSize;
			}
			AmmoCount.text = bulletsInMagzine + "/" + ammoPool;
		}, realoadTime);
	}

	void Update() {
		OverheatBar.value = heatLevel;
		if (beginCooldownCounter >= beginCooldownIn) {
			beginCooldown = true;
		}

		beginCooldownCounter += Time.deltaTime;
		
		if (Time.time > nextFire)
			WeaponReadyState = ReadyState.Ready;
	}

	public void OnEquip () {
		AmmoCount.text = bulletsInMagzine + "/" + ammoPool;
	}

	void FixedUpdate() {
		
	}
}
