﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	[SerializeField] LayerMask mask;
	[SerializeField] float bulletSpeed = 5;
	public Vector3 hitPoint = Vector3.zero;

	[HideInInspector()] public float Damage;

	Zombie zombieAtTarget;
	
	void Update () {
		if (isHitPointReached()) {
			Destroy(gameObject);
			if (zombieAtTarget != null) {
				GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).RaiseEvent ("BulletHitZombie");
				zombieAtTarget.TakeDamage (Damage);
				Destroy (this.gameObject);
			}
			return;	
		}

		transform.Translate(Vector3.right * bulletSpeed * Time.deltaTime);

		// if (hitPoint != Vector3.zero) {
		// 	return;	
		// }

		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.right, out hit, .2f, mask)) {
			CheckDestrucktable(hit);
		}
	}

	void CheckDestrucktable (RaycastHit hit) {
		Debug.Log ("Hit: " + hit.transform.name);
		hitPoint = hit.point + hit.normal * 0.0001f;
		
		// Check if zombie
		var zombie = hit.transform.GetComponent<Zombie>();
		if (zombie != null)
			zombieAtTarget = zombie;
	}

	bool isHitPointReached () {
		if (hitPoint == Vector3.zero)
			return false;

		Vector3 directionToHitPoint = hitPoint - transform.position;

		float dot = Vector3.Dot (directionToHitPoint, transform.right);
		//Debug.Log ("HitPoint: " + hitPoint + " Dot: " + dot);

		if (dot < 0)
			return true;

		return false;
	}
}
