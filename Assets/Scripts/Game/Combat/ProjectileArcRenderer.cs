﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (LineRenderer))]
public class ProjectileArcRenderer : MonoBehaviour {

	public float gravity;
	float radianAngle;
	float height;

	public float angle;
	public float velocity;
	[Range (10, 50)] public int resolution;
	public Vector3[] points;

	LineRenderer lineRenderer;

	Vector3[] calculateTrajectory () {
		points = new Vector3 [resolution + 1];
		radianAngle = Mathf.Deg2Rad * angle;

		float maximumDistance = (((velocity * velocity * Mathf.Sin (2 * radianAngle)) / (2 * gravity)) * 
															(1 + Mathf.Sqrt (1 + ((gravity * height) / 
															(velocity * velocity * Mathf.Sin (radianAngle) * Mathf.Sin (radianAngle))))));
		for (int i = 0; i <= resolution; i++) {
			float t = (float) i / (float) resolution;
			points [i] = heightAtPoint (t * maximumDistance);
		}

		return points;
	}

	Vector3 heightAtPoint (float distanceOnX) {
		float y = distanceOnX * Mathf.Tan (radianAngle) - 
								((gravity * distanceOnX * distanceOnX) / 
								(2 * velocity * velocity * Mathf.Cos (radianAngle) * Mathf.Cos (radianAngle)));
		return new Vector3 (distanceOnX, y);
	}

	void Awake () {
		lineRenderer = GetComponent <LineRenderer> ();
		gravity = Mathf.Abs (Physics.gravity.y);
		height = transform.position.y;
	}

	void Start () {
		lineRenderer.positionCount = resolution + 1;
		lineRenderer.SetPositions (calculateTrajectory ());
	}
	
	void Update () {
	}
}
