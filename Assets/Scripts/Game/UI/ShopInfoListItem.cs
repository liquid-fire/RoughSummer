﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopInfoListItem : MonoBehaviour {
	[SerializeField] Image weaponImage;
	[SerializeField] Text gunName;
	[SerializeField] Text ammoCount;

	public void SetUp (string firstLine, string secondLine) {
		gunName.text = firstLine;
		ammoCount.text = secondLine;
	}
}
