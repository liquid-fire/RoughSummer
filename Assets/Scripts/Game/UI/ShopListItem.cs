﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopListItem : MonoBehaviour {
	public Text Amount;
	public Text ItemName;
	public Button BuyButton;

	[SerializeField] GameItem holdItem;
	[SerializeField] int amount;
	[SerializeField] float pricePerItem;

	public void SetUp (GameItem item, int amount, float pricePerItem) {
		holdItem = item;
		this.amount = amount;
		this.pricePerItem = pricePerItem;
		Amount.text = "" + amount;
		ItemName.text = holdItem.GeneralType ();
		BuyButton.onClick.AddListener (buyPressed);
	}

	void buyPressed () {
		Shop shop = GameObject.Find ("Canvas/Shop").GetComponent<Shop> ();
		if (shop != null)
			shop.OnBuyPressed (holdItem, amount, amount * pricePerItem);
	}
}