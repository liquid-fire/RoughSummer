﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour {
	[SerializeField] GameObject GameUi;
	[SerializeField] GameObject ShopUi;
	[SerializeField] Button closeShop;
	[SerializeField] GameObject MainMenu;
	[SerializeField] GameObject PostPlay;

	void hideAll () {
		GameUi.SetActive (false);
		ShopUi.SetActive (false);
		MainMenu.SetActive (false);	
		PostPlay.SetActive (false);
	}

	public void ShowShop () {
		hideAll ();
		ShopUi.SetActive (true);
	}

	public void ShowGameHUD () {
		hideAll ();
		GameUi.SetActive (true);
	}

	public void ShowMainMenu () {
		hideAll ();
		MainMenu.SetActive (true);
	}

	public void ShowPostPlay () {
		hideAll ();
		PostPlay.SetActive (true);
	}

	void Awake() {
		closeShop.onClick.AddListener (() => {
			ShowGameHUD ();
			GameObject.Find ("Director").GetComponent<GameProgress> ().CloseShop ();
		});
	}
}
