﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour {
	[Header ("General")]
	[SerializeField] Button playButton;
	[SerializeField] Button exitButton;
	

	// UI elements
	[Header ("Google Play GameObject")]
	[SerializeField] GameObject NotSignedIn;
	[SerializeField] GameObject SignedIn;

	// Signed in UI
	[Header ("Signed In")]
	[SerializeField] Button achivement;
	[SerializeField] Text userName;
	[SerializeField] Image profilePic;

	// Not signed in UI
	[Header ("Not Signed In")]
	[SerializeField] Button signIn;

	PlayServiceInterface PlayServiceInterface {
		get {
			return GameManager.Instance.GetComponent<PlayServiceInterface> (EPlugInRef.PlayGame);
		}
	}

	void Awake () {
		GameManager.Instance.PlugInManager.LoadPlugin <PlayServiceInterface> (EPlugInRef.PlayGame);

		EventBus.EventListener playerSignedInListener = new EventBus.EventListener ();
		playerSignedInListener.Method = (object obj) => {
			bool signIn = (bool) obj;
			if (signIn) {
				NotSignedIn.SetActive (false);
				SignedIn.SetActive (true);
				setupSignedInUi ();
			} else {
				NotSignedIn.SetActive (true);
				SignedIn.SetActive (false);
			}
		};
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("GooglePlaySignIn", playerSignedInListener);

		GameObject.Find ("Canvas").GetComponent<UiManager> ().ShowMainMenu ();
	}

	void Start () {
		DontDestroyOnLoad (GameObject.Find ("_gameManager"));
		playButton.onClick.AddListener (() => {
			Debug.Log ("Play clicked, starting game");
			GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).RaiseEvent ("StartGame");
			GameObject.Find ("Canvas").GetComponent<UiManager> ().ShowGameHUD ();
		});

		exitButton.onClick.AddListener (() => {
			Application.Quit ();
		});

		checkSignInAndSetupUi ();

		signIn.onClick.AddListener (() => {
			PlayServiceInterface.SignInToGooglePlay ();
		});

		achivement.onClick.AddListener (() => {
			PlayServiceInterface.ShowLeaderboard ();
		});
	}

	void checkSignInAndSetupUi () {
		NotSignedIn.SetActive (false);
		SignedIn.SetActive (false);
		if (PlayerPrefs.GetInt ("signedin_once", 0) == 0) {
			NotSignedIn.SetActive (true);
			SignedIn.SetActive (false);
		} else if (PlayerPrefs.GetInt ("signedin_once", 0) == 1) {
			PlayServiceInterface.SignInToGooglePlay ();
		} else {
			NotSignedIn.SetActive (true);
			SignedIn.SetActive (false);
		}
	}

	void setupSignedInUi () {
		userName.text = PlayServiceInterface.GetUserName ();
		Texture2D image = PlayServiceInterface.GetProfileImage ();
		profilePic.sprite = Sprite.Create (image, new Rect (0, 0, image.width, image.height), new Vector2(0.5f, 0.5f));
	}
}
