﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PostPlayManager : MonoBehaviour {
	[SerializeField] Text score;
	[SerializeField] Button restart;

	void Awake () {
	}

	public void SetScore (int score) {
		this.score.text = score.ToString ();
	}

	public void ShowPostPlay () {
		GameObject.Find ("Canvas").GetComponent<UiManager> ().ShowPostPlay ();
	}
	
	void Start () {
		restart.onClick.AddListener (() => {
			Debug.Log ("Restart Game clicked");
			GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).RaiseEvent ("RestartGame");
			GameObject.Find ("Canvas").GetComponent<UiManager> ().ShowGameHUD ();
		});
	}
}
