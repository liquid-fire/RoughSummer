﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSystem : MonoBehaviour {

	public float BackgroundSize;
  //public float ParallaxSpeed;

  private Transform cameraTransform;
  public Transform[] layers;
  private float viewzone = 2;
  private int leftIndex;
  private int rightIndex;
  private float lastCameraX;

  void Start() {
    cameraTransform = Camera.main.transform;
    lastCameraX = cameraTransform.position.x;
    layers = new Transform [transform.childCount];

    for (int i = 0; i < transform.childCount; i++)
      layers [i] = transform.GetChild (i);
    
    leftIndex = 0;
    rightIndex = layers.Length - 1;
  }

  void Update() {
    if (cameraTransform.position.x < (layers[leftIndex].position.x + viewzone)) {
      scrollLeft ();
    }

    // if (cameraTransform.position.x > (layers[rightIndex].position.x - viewzone)) {
    //   scrollRight ();
    // }

    for (int i = 0; i < transform.childCount; i++) {
      Vector3 pos = layers [i].localPosition;
      pos.z = 0;
      layers [i].localPosition = pos;
    }
  }

  void scrollLeft () {
    int lastRight = rightIndex;
    layers [rightIndex].position = Vector2.right * (layers [leftIndex].position.x - BackgroundSize);

    leftIndex = rightIndex;
    rightIndex--;
    if (rightIndex < 0)
      rightIndex = layers.Length - 1;
  }

  void scrollRight () {
    int lastLeft = leftIndex;
    layers [leftIndex].position = Vector2.right * (layers [rightIndex].position.x + BackgroundSize);

    rightIndex = leftIndex;
    leftIndex++;
    if (leftIndex == layers.Length)
      leftIndex = 0;
  }
}
