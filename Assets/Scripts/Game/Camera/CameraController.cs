﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	Transform localPlayer;
	Camera currentCamera;

	[System.Serializable]
	public class CameraRig {
		public Vector3 CameraOffset;
		public float damping;
	}

	[SerializeField] public CameraRig defaultCameraRight;
	[SerializeField] Animator shakeAnimator;

	bool ready = false;

	void Awake() {
		EventBus.EventListener listener = new EventBus.EventListener ();
		listener.Method = (object obj) => {
			if (obj != null)
				localPlayer = (Transform) obj;
			else
				localPlayer = GameObject.Find ("Player").transform;
			currentCamera = Camera.main;
			if (localPlayer) {
				Debug.Log ("Found local player");
				ready = true;
			}
		};
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("PlayerReady", listener);

		EventBus.EventListener bulletHitZombie = new EventBus.EventListener ();
		bulletHitZombie.IsSingleShot = false;
		bulletHitZombie.Method = (object obj) => {
			shakeAnimator.SetTrigger ("shake");
		};
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("BulletHitZombie", bulletHitZombie);

		EventBus.EventListener genadeExploded = new EventBus.EventListener ();
		genadeExploded.IsSingleShot = false;
		genadeExploded.Method = (object obj) => {
			//shakeAnimator.SetTrigger ("explosion");
		};
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("GrenadeExploded", genadeExploded);

		EventBus.EventListener playerTookDamage = new EventBus.EventListener ();
		playerTookDamage.IsSingleShot = false;
		playerTookDamage.Method = (object obj) => {
			Debug.Log ("Shake camera, player taking dmg");
			shakeAnimator.SetTrigger ("shake01");
		};
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("PlayerTookDamage", playerTookDamage);
	}

	void Start () {}
	
	void LateUpdate () {
		if (!ready)
			return;
		
		CameraRig currentCamera = defaultCameraRight;

		Vector3 targetPosition = 
			localPlayer.transform.position + localPlayer.transform.right * currentCamera.CameraOffset.x
			+ localPlayer.forward * currentCamera.CameraOffset.z
			+ localPlayer.up * currentCamera.CameraOffset.y;

		transform.parent.position = Vector3.Lerp(transform.parent.position, targetPosition, currentCamera.damping * Time.deltaTime);
	}
}
