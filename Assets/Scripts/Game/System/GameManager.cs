﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager {
	private GameObject gameObject;
	private static GameManager m_Instance;
	private PluginManager m_PlugInManager;
	public PluginManager PlugInManager {
		get {
			if (m_PlugInManager == null) 
				m_PlugInManager = new PluginManager ();
			return m_PlugInManager;
		}
	}

	public Player Player;
	
	public static GameManager Instance {
		get {
			if (m_Instance == null) {
				m_Instance = new GameManager ();
				m_Instance.gameObject = new GameObject("_gameManager");

				// Load Global Plugins
				m_Instance.PlugInManager.LoadPlugin <EventBus> (EPlugInRef.EventBus);

				EventBus.EventListener playerReadyListener = new EventBus.EventListener ();
				playerReadyListener.Method = (object obj) => {
					m_Instance.Player = GameObject.Find("Player").GetComponent<Player> ();
				};
				m_Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("PlayerReady", playerReadyListener);
			}

			return m_Instance;
		}
	}
	
	public Dictionary<EPlugInRef, GameManagerPlugin> PlugInData {
		get {
			return m_PlugInManager.activePlugIn;
		}
	}

	public T GetComponent<T> (EPlugInRef reference) where T : GameManagerPlugin {
		return (T) m_PlugInManager.GetComponent (reference);
	}

	public void RunResetRoutine () {
		foreach (GameManagerPlugin plugin in m_PlugInManager.activePlugIn.Values) {
			plugin.ResetRoutine ();
		}
	}

	public class PluginManager {
		public Dictionary<EPlugInRef, GameManagerPlugin> activePlugIn;

		public PluginManager () {
			activePlugIn = new Dictionary<EPlugInRef, GameManagerPlugin> ();
		}

		public GameManagerPlugin GetComponent (EPlugInRef reference) {
			if (activePlugIn.ContainsKey (reference)) {
				return activePlugIn [reference];
			}

			return null;
		}

		public void LoadPlugin<T> (EPlugInRef reference) where T : GameManagerPlugin {
			T t = m_Instance.gameObject.AddComponent<T> ();
			t.OnLoad ();
			t.Ref = reference;
			activePlugIn.Add (reference, t);
		}

		public void RemovePlugin<T> () where T : GameManagerPlugin {
			T t = m_Instance.gameObject.GetComponent<T> ();
			activePlugIn.Remove (t.Ref);
			if (t != null)
				t.SelfDestruct ();
		}
	}
}
