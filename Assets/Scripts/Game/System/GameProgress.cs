﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;

public class GameProgress : MonoBehaviour {
	[SerializeField] GameState gameState;
	[SerializeField] Text centerMessage;
	[SerializeField] GameObject mainMenu;
	[SerializeField] GameObject postPlay;
	[SerializeField] ZombieWaveLogic zombieWaveLogic = new ZombieWaveLogic ();
	public ZombieWaveLogic ZombieWave {
		get {
			return zombieWaveLogic;
		}
	}
	[SerializeField] ShopLogic shopLogic = new ShopLogic ();
	Transform localPlayer;
	UiManager uiManager;
	public bool spawnZombie = false;
	public static Vector3 ZombieSpawnPoint;
	
	Transform initalPlayerTransform;
	Transform initialCameraTransfom;

	public enum GameState {
		MainMenu,
		Playing,
		Conlusion,
	}

	void Awake() {
		// Load

		initalPlayerTransform = GameObject.Find ("Player").transform;
		initialCameraTransfom = GameObject.Find ("CameraAnchor").transform;

		GameManager.Instance.PlugInManager.LoadPlugin <InputManager> (EPlugInRef.InputManager);
		GameManager.Instance.PlugInManager.LoadPlugin <Timer> (EPlugInRef.Timer);
		GameManager.Instance.PlugInManager.LoadPlugin <ScoreKeeper> (EPlugInRef.ScoreKeeper);
		GameManager.Instance.PlugInManager.LoadPlugin <Inventory> (EPlugInRef.Inventory);

		uiManager = GameObject.Find ("Canvas").GetComponent<UiManager> ();

		EventBus.EventListener playerReadyListener = new EventBus.EventListener ();
		playerReadyListener.Method = (object obj) => {
			if (obj != null)
				localPlayer = (Transform) obj;
			else
				localPlayer = GameObject.Find ("Player").transform;
		};
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("PlayerReady", playerReadyListener);

		EventBus.EventListener playerDiedListener = new EventBus.EventListener ();
		playerDiedListener.IsSingleShot = false;
		playerDiedListener.Method = (object obj) => {
			spawnZombie = false;
			GameManager.Instance.GetComponent <PlayServiceInterface> (EPlugInRef.PlayGame).SendScoreToLeaderboard (
				GameManager.Instance.GetComponent <ScoreKeeper> (EPlugInRef.ScoreKeeper).CurrentScore
			);

			gameState = GameState.Conlusion;
			stateChange ();
			//SceneManager.LoadSceneAsync ("game_over");
		};
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("PlayerDied", playerDiedListener); 

		EventBus.EventListener waveInitiateListener = new EventBus.EventListener ();
		waveInitiateListener.IsSingleShot = false;
		waveInitiateListener.Method = (object obj) => {
			centerMessage.text = "Wave " + (int) obj;
			GameManager.Instance.GetComponent<Timer> (EPlugInRef.Timer).Add (() => {
				centerMessage.text = "";
			}, 1f);
		};
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("WaveInitiated", waveInitiateListener);

		EventBus.EventListener waveCompleteListener = new EventBus.EventListener ();
		waveCompleteListener.IsSingleShot = false;
		waveCompleteListener.Method = (object obj) => {
			// show shop
			// start new wave
			Debug.Log ("Wave Over");
			centerMessage.text = "Wave over opening shop";
			GameManager.Instance.GetComponent<Timer> (EPlugInRef.Timer).Add (() => {
				centerMessage.text = "";
				GameManager.Instance.GetComponent<InputManager> (EPlugInRef.InputManager).TakeInput = false;
				shopLogic.ShowShop (uiManager);
				GameManager.Instance.GetComponent<Timer> (EPlugInRef.Timer).Add (CloseShop, 10f, "shop_timer");
			}, 4);
		};
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("WaveComplete", waveCompleteListener);

		EventBus.EventListener restartGameListener = new EventBus.EventListener ();
		restartGameListener.IsSingleShot = false;
		restartGameListener.Method = (object obj) => {
			Debug.Log ("Initiating game restart sequence");
			gameState = GameState.Playing;
			localPlayer.GetComponent <Player> ().UnReadyPlayer ();
			localPlayer.gameObject.SetActive (true);
			Debug.Log ("Running reset routines");
			GameManager.Instance.RunResetRoutine ();
			zombieWaveLogic.RunResetRoutine ();
			zombieWaveLogic.ReadyZombieLogic ();
			spawnZombie = true;
			//localPlayer.position = initalPlayerTransform.position;
			//GameObject.Find ("CameraAnchor").transform.position = initialCameraTransfom.position;
			foreach (GameObject zombie in GameObject.FindGameObjectsWithTag ("Zombie")) {
				Destroy (zombie);
			}
			stateChange ();
		};
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("RestartGame", restartGameListener);

		// EventBus.EventListener shopClosedListenter = new EventBus.EventListener ();
		// shopClosedListenter.IsSingleShot = false;
		// shopClosedListenter.Method = (object obj) => {
			
		// };
		// GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("ShopClosed", shopClosedListenter);

		EventBus.EventListener startGameListener = new EventBus.EventListener ();
		startGameListener.IsSingleShot = false;
		startGameListener.Method = (object obj) => {
			gameState = GameState.Playing;
			stateChange ();
		};
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("StartGame", startGameListener);

		stateChange ();
	}

	public void CloseShop () {
		if (shopLogic.OpenState == ShopLogic.ShopOpenedState.Closed)
			return;
		
		GameManager.Instance.GetComponent<Timer> (EPlugInRef.Timer).RemoveEvent ("shop_timer");
		shopLogic.HideShop (uiManager);
		zombieWaveLogic.ReadyZombieLogic ();
		zombieWaveLogic.InitiateNextWave ();
	}

	void stateChange () {
		if (gameState == GameState.MainMenu) {
			mainMenu.SetActive (true);
		}

		if (gameState == GameState.Playing) {
			if (localPlayer == null)
				GameObject.Find ("Player").GetComponent <Player> ().ReadyPlayer ();
			else 
				localPlayer.GetComponent <Player> ().ReadyPlayer ();
			if (spawnZombie)
				zombieWaveLogic.InitiateNextWave ();
		}

		if (gameState == GameState.Conlusion) {
			postPlay.SetActive (true);
			postPlay.GetComponent <PostPlayManager> ().ShowPostPlay ();
			postPlay.GetComponent <PostPlayManager> ().SetScore (
				GameManager.Instance.GetComponent <ScoreKeeper> (EPlugInRef.ScoreKeeper).CurrentScore
			);
		}
	}

	void updateZombieSpawnPoint () {
		//Vector3 cameraPos = Camera.main.ViewportToWorldPoint (new Vector3 (.5f, .5f, .5f)); Camera.main.farClipPlane
		Vector3 frustrumEdge = Camera.main.ViewportToWorldPoint (new Vector3 (1.2f, 0.5f, Vector3.Distance(localPlayer.position, Camera.main.transform.position)));

		frustrumEdge.y = 0.103f;
		frustrumEdge.z = localPlayer.position.z;
		ZombieSpawnPoint = frustrumEdge;
	}
	
	void Update () {
		if (gameState == GameState.Playing) {
			updateZombieSpawnPoint ();
			zombieWaveLogic.Update ();
		}
	}

	[System.Serializable]
	public class ShopLogic {
		UiManager ui;
		public enum ShopOpenedState {
			Opened,
			Closed,
		}

		public enum ShopVisibleState {
			Visible,
			Invisible
		}
		
		[SerializeField] [Range (1f, 10f)] float shopOpenTime;
		[SerializeField] ShopOpenedState openState = ShopOpenedState.Closed;
		public ShopOpenedState OpenState {
			get {
				return openState;
			}
		}
		[SerializeField] ShopVisibleState visibleState = ShopVisibleState.Invisible;

		public void ShowShop (UiManager ui) {
			ui.ShowShop ();
			GameObject.Find("Canvas/Shop").GetComponent<Shop> ().UpdateUi ();
			openState = ShopOpenedState.Opened;
			visibleState = ShopVisibleState.Visible;
			Debug.Log ("Show shop");
		}

		public void HideShop (UiManager ui) {
			ui.ShowGameHUD ();
			openState = ShopOpenedState.Closed;
			visibleState = ShopVisibleState.Invisible;
			GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).RaiseEvent ("ShopClosed");
		}
	}

	[System.Serializable]
	public class ZombieWaveLogic {
		public enum WaveState {
			Stopped,							// state after complete when all the zombies are killed
			Complete,							// state when the spawning is done but still waiting for the zombies to be killed
			Spawning,
			Ready,								// state after the shop is closed
		}

		[SerializeField] WaveState waveState = WaveState.Stopped;
		public WaveState State {
			get {
				return waveState;
			}
		}
		int waveAngleCounter = 1;
		public int CurrentWave {
			get; private set;
		}
		float toSpawnInCurrentWave;
		int currentlyAliveZombies;
		float nextSpawnIn;
		int spawnedInCurrentWave;

		[SerializeField] Zombie zombie;
		[SerializeField] [Range (0.000f, 2.000f)]float spawnIn = .5f;
		[SerializeField] [Range (.6f, 100)] float zombieSpawnMultiplier = 1;
		[SerializeField] Dictionary<Zombie, bool> zombieListForCurrentWave = new Dictionary<Zombie, bool> ();

		public ZombieWaveLogic () {
		}

		public void RunResetRoutine () {
			CurrentWave = 0;
			toSpawnInCurrentWave = 0;
			nextSpawnIn = 0;
			spawnedInCurrentWave = 0;
			zombieListForCurrentWave.Clear ();
		}

		public void Update () {
			if (waveState == WaveState.Stopped) 
				return;

			if (waveState == WaveState.Spawning) {
				if (Time.time > nextSpawnIn) {
					Zombie newZombie = Instantiate (zombie, GameProgress.ZombieSpawnPoint, Quaternion.Euler (0, -90, 0));
					zombieListForCurrentWave.Add (newZombie, true);
					spawnedInCurrentWave ++;
					nextSpawnIn = Time.time + spawnIn;
					if (spawnedInCurrentWave == toSpawnInCurrentWave) {
						waveState = WaveState.Complete;
					}
				}
			}
			
			checkZombieDeath ();
		}

		void checkZombieDeath () {
			foreach (var item in zombieListForCurrentWave) {
				if (item.Value == true) {
					currentlyAliveZombies ++;
					if (!item.Key.IsAlive) 
						zombieListForCurrentWave[item.Key] = false;
				}
			}

			if (waveState == WaveState.Complete) {
				var aliveZombies = from zom in zombieListForCurrentWave where zom.Value == true select zom;
				if (aliveZombies.Count () == 0) {
					Debug.Log ("Raising wave complete");
					GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).RaiseEvent ("WaveComplete");
					waveState = WaveState.Stopped;
				}
			}
		}

		public void ReadyZombieLogic () {
			waveState = WaveState.Ready;
		}

		public void InitiateNextWave () {
			CurrentWave++;
			GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).RaiseEvent ("WaveInitiated", CurrentWave);
			spawnedInCurrentWave = 0;

			foreach (var item in zombieListForCurrentWave) {
				Destroy (item.Key.gameObject);
			}

			zombieListForCurrentWave.Clear ();
			Debug.Log ("Multiplier: " + zombieSpawnMultiplier + " Current wave: " + CurrentWave);
			float waveMulWithMultiplier = zombieSpawnMultiplier * CurrentWave;
			toSpawnInCurrentWave = Mathf.Round (waveMulWithMultiplier);
			Debug.Log ("Current wave will have " + toSpawnInCurrentWave + " zombies");
			currentlyAliveZombies = 0;
			waveAngleCounter++;
			waveState = WaveState.Spawning;

			if (waveAngleCounter >= 4)
				waveAngleCounter = 1;

			GameManager.Instance.GetComponent<InputManager> (EPlugInRef.InputManager).TakeInput = true;
		}
	}
}