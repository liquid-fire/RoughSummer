﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputManager : GameManagerPlugin {
	Vector2 touchOrigin = -Vector2.one;

	private bool stepBack;
	public bool StepBack {
		get {
			return stepBack;
		}
	}

	private bool fire;
	public bool Fire {
		get {
			return fire;
		}
	}

	private bool throwNade;
	public bool ThrowNade {
		get {
			return throwNade;
		}
		set {
			throwNade = value;
		}
	}

	EventSystem eventSystem;

	Button throwNadeButton;
	bool canThrowNade = true;

	public override void OnLoad() {
		eventSystem = GameObject.Find ("EventSystem").GetComponent<EventSystem> ();
		throwNadeButton = GameObject.Find ("Canvas/GameUi/GrenadeThrowButton").GetComponent<Button> ();
		throwNadeButton.onClick.AddListener (() => {
			if (canThrowNade) {
				throwNade = true;
				canThrowNade = false;
				Debug.Log ("Input Manager: Throw Nade");
				GameManager.Instance.GetComponent <Timer> (EPlugInRef.Timer).Add (() => {
					canThrowNade = true;
				}, 2f);
			}
		});
	}

	bool canFire;
	float canFireIn;
	public bool TakeInput = false;

	void Update () {
		if (!TakeInput)
			return;

		#if UNITY_EDITOR
		
		if (Input.GetKey (KeyCode.Space)) {
			fire = true;
		} else {
			fire = false;
		}

		if (Input.GetKey (KeyCode.LeftArrow)) {
			stepBack = true;
		} else {
			stepBack = false;
		}

		#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE

		if (Input.touchCount > 0) {
			// Take only first touch, ignore rest.

			Touch touch = Input.touches [0];
			if (EventSystem.current.IsPointerOverGameObject(touch.fingerId)) {
        // you touched at least one UI element
				Debug.Log ("Pressed a button");
				stepBack = false;
				fire = false;
				canFireIn = Time.time + .4f;  // this is a hack, but it works, so basically it fails when leave the button
																			// to counter that we use this cooldown method for fire. 
      } else {
				if (Time.time > canFireIn)
					processInput (touch.position);
			}
		} else {
			stepBack = false;
			fire = false;
		}

		#endif
	}

	void processInput (Vector2 touchPosition) {
		float screenWidth = Screen.currentResolution.width;
		if (touchPosition.x < screenWidth / 2) {
			stepBack = true;
			fire = false;
		} else {
			stepBack = false;
			fire = true;
		}
	}

  public override void ResetRoutine() {}
}
