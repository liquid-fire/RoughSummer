using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : GameManagerPlugin {

	public class TimedEvent {
		public float TimeToExecute;
		public Callback Method;
		public OnTick OnTick;
		public string Tag;
		public float Tick;
		public float LastTime;
		public bool loop;

		public TimedEvent () {
			Tag = "tag";
			LastTime = Time.time;
			loop = false;
		}
	}

	public delegate void Callback ();
	public delegate void OnTick ();

	public List<TimedEvent> events;

	void Awake () {
		events = new List<TimedEvent> ();
	}

	public void Add (Callback method, float inSecond) {
		events.Add (new TimedEvent {
			Method = method,
				TimeToExecute = Time.time + inSecond
		});
	}

	public void Add (Callback method, float inSecond, string tag) {
		events.Add (new TimedEvent {
			Method = method,
				TimeToExecute = Time.time + inSecond,
				Tag = tag
		});
	}

	public void Add (Callback method, OnTick onTick, float inSecond, float tick, string tag) {
		Debug.Log (tag + " event added");
		events.Add (new TimedEvent {
			Method = method,
				OnTick = onTick,
				TimeToExecute = Time.time + inSecond,
				Tick = tick,
				Tag = tag
		});
	}

	public void Add (OnTick onTick, float tick, string tag, bool loop = true) {
		Debug.Log (tag + " event added");
		events.Add (new TimedEvent {
			OnTick = onTick,
				Tick = tick,
				Tag = tag,
				loop = true
		});
	}

	public bool RemoveEvent (string tag) {
		for (int i = 0; i < events.Count; i++) {
			var timedEvent = events[i];
			if (timedEvent.Tag == tag) {
				Debug.Log ("Removing Timer Event: " + timedEvent.Tag);
				events.Remove (timedEvent);
			}
			return true;
		}

		return false;
	}

	public bool HasEvent (string tag) {
		for (int i = 0; i < events.Count; i++) {
			var timedEvent = events[i];
			if (timedEvent.Tag.CompareTo (tag) == 0)
				return true;
		}

		return false;
	}
	void Update () {
		if (events.Count == 0)
			return;

		for (int i = 0; i < events.Count; i++) {
			var timedEvent = events[i];
			if (timedEvent.TimeToExecute <= Time.time && !timedEvent.loop) {
				try {
					timedEvent.Method ();
					events.Remove (timedEvent);
					continue;
				} catch (MissingReferenceException) {
					events.Remove (timedEvent);
				}
			}

			if (timedEvent.OnTick != null) {
				if (Time.time - timedEvent.LastTime >= timedEvent.Tick) {
					timedEvent.LastTime = Time.time;
					timedEvent.OnTick ();
				}
			}
		}
	}

  public override void ResetRoutine() {}
}