﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class PlayServiceInterface : GameManagerPlugin {

	public bool IsLogedIn {
		get {
			return Social.localUser.authenticated;
		}
	}

	public void SignInToGooglePlay () {
		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
		.RequestEmail()
		.Build();
		PlayGamesPlatform.InitializeInstance(config);
		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.Activate();
		
		Social.localUser.Authenticate((bool success) => {					// temp code
			if (success) {
				Debug.Log ("user signed in");
				PlayerPrefs.SetInt ("signedin_once", 1);
			} else {
				Debug.Log ("user not signed in");
				PlayerPrefs.SetInt ("signedin_once", -1);
			}

			PlayerPrefs.Save ();

			GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).RaiseEvent ("GooglePlaySignIn", success);
    });
	}

	public void SendScoreToLeaderboard (int score) {
		if (IsLogedIn) {
			Social.ReportScore (score, PlayGamesServiceIds.leaderboard_top_score, (bool success) => {
				if (success) {
					Debug.Log ("Scores were posted");
				} else {
					Debug.Log ("Failed to post the scores");
				}
			});
		}
	}

	public void ShowAchievement () {
		Social.ShowAchievementsUI();
	}

	public void ShowLeaderboard () {
		Social.ShowLeaderboardUI();
	}

	public string GetUserName () {
		return Social.localUser.userName;
	}

	public Texture2D GetProfileImage () {
		return Social.localUser.image;
	}

  public override void ResetRoutine() {}
}
