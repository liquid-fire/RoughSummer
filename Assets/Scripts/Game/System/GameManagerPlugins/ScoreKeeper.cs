﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreKeeper : GameManagerPlugin {
	private int currentScore;
	public int CurrentScore {
		get {
			return currentScore;
		}
	}

	private float currentCredit;
	public float CurrentCredit {
		get {
			return currentCredit;
		}
	}

	private int currentMultiplyer = 1;

	public Text scoreText;

	public override void OnLoad() {
		currentScore = 0;
		scoreText = GameObject.Find ("Canvas/GameUi/Score").GetComponent<Text> ();
		if (scoreText == null) {
			Debug.Log ("Score text is null");
		} else {
			updateUi ();
		}
	}

	void Awake() {
		EventBus.EventListener zombieDeathListener = new EventBus.EventListener ();
		zombieDeathListener.IsSingleShot = false;
		zombieDeathListener.Method = (object obj) => {
			List <float> pointCredit = (List<float>) obj;
			incrementScoreCredit ((int) pointCredit[0], pointCredit[1]);
		};
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("ZombieDied", zombieDeathListener);

		EventBus.EventListener shopClosedListenter = new EventBus.EventListener ();
		shopClosedListenter.IsSingleShot = false;
		shopClosedListenter.Method = (object obj) => {
			updateUi ();
		};
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("ShopClosed", shopClosedListenter);
	}

	void incrementScoreCredit (int pointsForKillingZombie, float creditEarnedForKillingZombie) {
		currentScore += pointsForKillingZombie * currentMultiplyer;
		currentCredit += creditEarnedForKillingZombie;
		updateUi ();
	}

	public void GiveCredit (int amount) {
		currentCredit -= amount;
	}

	void updateUi () {
		scoreText.text = "Score: " + currentScore + " Credit: " + currentCredit; // Damn I'm lazy xD 
	}
	
	void Start () {
		currentScore = 0;
	}

  public override void ResetRoutine() {
    currentCredit = 0;
		currentScore = 0;
		updateUi ();
  }
}
