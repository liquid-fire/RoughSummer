﻿public enum EPlugInRef {
	InputManager,
	Timer,
	EventBus,
	ScoreKeeper,
	Inventory,
	PlayGame,
}
