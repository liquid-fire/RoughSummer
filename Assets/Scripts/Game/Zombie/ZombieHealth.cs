﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieHealth : HitPoint {

	public override void Die() {
		base.Die();
		//Destroy (gameObject);
		gameObject.SetActive (false);
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).RaiseEvent ("ZombieDied");
	}

	public override float TakeDamage (float amount) {
		if (!IsAlive)
			return -1;
		
		base.TakeDamage (amount);

		if (!IsAlive) {
			Die ();
		}

		return amount;
	}

	void Start () {
		
	}
	
	void Update () {
		
	}
}
