﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Zombie : HitPoint {
	[SerializeField] [Range (0, 3)] float stunTime;
	[Header ("General")]
	[SerializeField] ZombieType type;
	[SerializeField] float movementSpeed;
	[SerializeField] float pointsGivenWhenKilled;
	[SerializeField] float creditsGivenWhenKilled;

	// [SerializeField] ZombieHealth zombieHealth;
	// public ZombieHealth Health {
	// 	get {
	// 		return zombieHealth;
	// 	}
	// }
	

	[Header ("Attack")]
	[SerializeField] [Range (10, 50)] float damage;
	[SerializeField] [Range (0.1f, 4)] float attacksPerSecond;

	bool moveForward = true;
	bool stuned = false;
	bool attack = false;
	float nextAttack;
	Player player;

	public enum ZombieType {
		Normal,
		Runner,
		HighHP,
		Suicidal
	}

	// public void TakeDamage (float amt) {
	// 	zombieHealth.TakeDamage (amt);
		
	// }

	public override float TakeDamage (float amount) {
		if (!IsAlive)
			return -1;

		stuned = true;
		GameManager.Instance.GetComponent<Timer> (EPlugInRef.Timer).Add (() => {
			stuned = false;
		}, stunTime);
		
		base.TakeDamage (amount);

		if (!IsAlive) {
			Die ();
		}

		return amount;
	}

	public override void Die() {
		base.Die();
		//Destroy (gameObject);
		gameObject.SetActive (false);
		List<float> pointsCredit = new List<float> ();
		pointsCredit.Add (pointsGivenWhenKilled);
		pointsCredit.Add (creditsGivenWhenKilled);
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).RaiseEvent ("ZombieDied", pointsCredit);
	}

	void Awake() {
		EventBus.EventListener playerDiedListener = new EventBus.EventListener ();
		playerDiedListener.Method = (object obj) => {
			moveForward = false;
			attack = false;
		};
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("PlayerDied", playerDiedListener); 
	}

	public void InAttackRange (Player player) {
		Debug.Log ("In range");
		moveForward = false;
		attack = true;
		this.player = player;
	}

	public void LeavingAttackRange () {
		Debug.Log ("Out of range");
		moveForward = true;
		attack = false;
	}
	void OnTriggerEnter(Collider other) {
		Transform transform = other.transform;
		if (transform.GetComponent<Player> ()) {
			InAttackRange (transform.GetComponent<Player> ());
		}
	}

	void move () {
		Vector3 moveVector = (transform.right * movementSpeed);
    transform.Translate(moveVector * Time.deltaTime);
	}

	void hit () {
		player.TakeDamage (damage);
		nextAttack = Time.time + (1/attacksPerSecond);
	}

	void Update () {
		if (!IsAlive) 
			return;

		if (stuned)
			return;

		if (moveForward) {
			move ();
		}

		if (attack) {
			if (Time.time > nextAttack)
				hit ();
		}
	}

	public class ZombieDeathPoints {
		public int points;
	}
}
