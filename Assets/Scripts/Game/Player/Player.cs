﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
	[SerializeField] Weapon weapon;
	[SerializeField] PlayerHealth playerHealth;
	[SerializeField] Animator animator;

	[Header ("Grenade")]
	[SerializeField] Transform grenadeSpawnPoint;
	[SerializeField] GameObject grenade;
	[SerializeField] LayerMask mask;

	public DefaultInventory Inventory = new DefaultInventory ();

	bool ready = false;

	[System.Serializable]
	public class DefaultInventory {
		[SerializeField] int rifleAmmo;

		[SerializeField] int handGunAmmo;

		[SerializeField] int shotGunShell;

		public int GrenadeCount;

		public int GetAmmoFor (Ammo.AmmoType type) {
			if (type == Ammo.AmmoType.RifleAmmo) {
				return rifleAmmo;
			} else if (type == Ammo.AmmoType.HandGunAmmo) {
				return handGunAmmo;
			} else if (type == Ammo.AmmoType.ShotGunShell) {
				return shotGunShell;
			} else {
				return 0;
			}
		}
	}

	bool firing;
	public float distanceFromFirstZombie;

	Inventory CurrentInventory {
		get {
			return GameManager.Instance.GetComponent <Inventory> (EPlugInRef.Inventory);
		}
	}

	public InputManager InputManager {
		get {
			return GameManager.Instance.GetComponent<InputManager> (EPlugInRef.InputManager);
		}
	}

	public Timer Timer {
		get {
			return GameManager.Instance.GetComponent<Timer> (EPlugInRef.Timer);
		}
	}

	public void TakeDamage (float amt) {
		playerHealth.TakeDamage (amt);
	}

	void Start () {
	}

	public void ReadyPlayer () {
		playerHealth.Reset ();
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).RaiseEvent ("PlayerReady", this.transform);
		weapon = GameManager.Instance.GetComponent<Inventory> (EPlugInRef.Inventory).Weapons.EquipNext ();
		ready = true;
	}

	public void UnReadyPlayer () {
		ready = false;
	}

	float nextMoveIn;
	
	void Update () {
		if (!playerHealth.IsAlive)
			return;

		if (!ready)
			return;

		checkDistanceFromZombie ();

		if (InputManager.StepBack) {
			if (distanceFromFirstZombie > 2.5) {
				nextMoveIn = Time.time + .4f;
			} else {
				if (Time.time > nextMoveIn) {
					Vector3 moveVector = (-transform.right);
    			transform.Translate(moveVector * Time.deltaTime);
					animator.SetBool ("walking", true);
				}
			}
		} else {
			animator.SetBool ("walking", false);
		}

		if (InputManager.Fire) {
			weapon.Fire ();
			Timer.RemoveEvent ("revert_weapon_status");
			firing = true;
			//animator.SetTrigger ("shoot");
		} else {
			firing = false;
			weapon.WeaponFiringState = Weapon.FiringState.NotFiring;
			weapon.NotFire ();
		}

		if (InputManager.ThrowNade) {
			InputManager.ThrowNade = false;
			throwNade ();
		}
	}

	void throwNade () {
		if (CurrentInventory.Grenades.Count != 0) {
			Instantiate (grenade, grenadeSpawnPoint.position, Quaternion.identity);
			CurrentInventory.Grenades.Throw ();
			Debug.Log ("Throwing");
		} else {
			Debug.Log ("No nades in the inventory");
		}
	}

	// void OnTriggerEnter(Collider other) {
	// 	Transform transform = other.transform;
	// 	if (transform.GetComponent<Zombie> ()) {
	// 		transform.GetComponent<Zombie> ().InAttackRange ();
	// 	}
	// }

	void OnTriggerExit(Collider other) {
		Transform transform = other.transform;
		if (transform.GetComponent<Zombie> ()) {
			transform.GetComponent<Zombie> ().LeavingAttackRange ();
		}
	}

	void checkDistanceFromZombie () {
		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.right, out hit, 1000f, mask)) {
			var zombie = hit.transform.GetComponent<Zombie>();
			if (zombie != null) {
				distanceFromFirstZombie = Vector2.Distance (transform.position, hit.transform.position);
			}
		}
	}

	[ContextMenu ("Next Weapon")]
	void NextWeapon () {
		weapon = GameManager.Instance.GetComponent<Inventory> (EPlugInRef.Inventory).Weapons.EquipNext ();
	}
}
