﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : HitPoint {

	[Header ("UI")]
	[SerializeField] Slider healthBar;

	public override void Die() {
		base.Die();
		gameObject.SetActive (false);
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).RaiseEvent ("PlayerDied");
	}

	public override float TakeDamage (float amount) {
		if (!IsAlive)
			return -1;

		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).RaiseEvent ("PlayerTookDamage");
		
		base.TakeDamage (amount);
		healthBar.value = HitPointsRemaining;

		if (!IsAlive) {
			Die ();
		}

		return amount;
	}

	public override void Reset() {
		base.Reset ();
		healthBar.maxValue = HitPoints;
		healthBar.value = HitPoints;
	}

	void Start () {
		healthBar.maxValue = HitPoints;
		healthBar.value = HitPoints;
	}
}
