﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : GameManagerPlugin {
	/*
		Keeps the record of GameItems the player holds.

 */

	WeaponSystem weaponSystem;
	public WeaponSystem Weapons {
		get {
			if (weaponSystem == null)
				weaponSystem = new WeaponSystem ();
			
			return weaponSystem;
		}
	}

	AmmoSystem ammoSystem;
	public AmmoSystem Ammo {
		get {
			if (ammoSystem == null)
				ammoSystem = new AmmoSystem ();

			return ammoSystem;
		}
	}

	GrenadeSystem grenadeSystem;
	public GrenadeSystem Grenades {
		get {
			if (grenadeSystem == null) {
				grenadeSystem = new GrenadeSystem ();
			}

			return grenadeSystem;
		}
	}

	public override void OnLoad() {
		EventBus.EventListener listener = new EventBus.EventListener ();
		listener.Method = (object obj) => {
			Player.DefaultInventory Inventory = GameManager.Instance.Player.Inventory;
			Ammo.Init (Inventory);
			Grenades.Init (GameManager.Instance.Player.Inventory);
			GameObject playerObject = GameObject.Find ("Player");
			Weapon[] weaponList = playerObject.transform.GetComponentsInChildren<Weapon> (true);
			Weapons.Init (weaponList);
		};
		GameManager.Instance.GetComponent<EventBus> (EPlugInRef.EventBus).ListenTo ("PlayerReady", listener);
	}

  public override void ResetRoutine() {
		ammoSystem = new AmmoSystem ();
		weaponSystem = new WeaponSystem ();
		grenadeSystem = new GrenadeSystem ();
    OnLoad ();
  }

  public class AmmoSystem {
		Dictionary<Ammo.AmmoType, int> ammoPool = new Dictionary<Ammo.AmmoType, int> ();
		
		public void Init (Player.DefaultInventory Inventory) {
			for (int i = 0; i < 3; i ++) {
				ammoPool.Add ((Ammo.AmmoType) i, Inventory.GetAmmoFor ((Ammo.AmmoType) System.Enum.ToObject (typeof(Ammo.AmmoType), i)));
			}
		}

		public int AmmoCount (Ammo.AmmoType type) {
			if (ammoPool.ContainsKey(type))
				return ammoPool[type];
			return 0;
		}

		public void SetAmmo (int amount, Ammo.AmmoType type) {
			if (ammoPool.ContainsKey(type))
				ammoPool[type] = amount;
		}

		public void AddAmmo (int amount, Ammo.AmmoType type) {
			if (ammoPool.ContainsKey(type))
				ammoPool[type] += amount;
		}

		public void TakeAmmo (int amount, Ammo.AmmoType type) {
			if (ammoPool.ContainsKey(type))
				ammoPool[type] -= amount;
		}
	}

	public class WeaponSystem {
		Dictionary <Weapon.WeaponType, Weapon> weapons = new Dictionary<Weapon.WeaponType, Weapon> ();
		public Dictionary <Weapon.WeaponType, Weapon> GetWeaponInfo {
			get {
				return weapons;
			}
		}
		int weaponCount = 0;
		int currentlyEquiped = -1;

		public void Init (Weapon[] weaponList) {
			foreach (var weapon in weaponList) {
				weapons.Add (weapon.Type, weapon);
				weaponCount ++;
			}
		}

		void DequipAll () {
			foreach (var item in weapons) {
				item.Value.gameObject.SetActive (false);
			}
		}

		Weapon EquipAt (int index) {
			int counter = 0;
			foreach (var item in weapons.Values) {
				if (index == counter) {
					item.gameObject.SetActive (true);
					item.OnEquip ();
					return item;
				}

				counter ++;
			}

			return null;
		}

		public Weapon EquipNext () {
			DequipAll ();
			if (currentlyEquiped == -1) {
				currentlyEquiped = 0;
				return EquipAt (0);
			} else {
				currentlyEquiped ++;
				if (currentlyEquiped >= weaponCount)
					currentlyEquiped = 0;
				return EquipAt (currentlyEquiped);
			}
		}
	}

	public class GrenadeSystem {
		int grenadeCount;
		public int Count {
			get {
				return grenadeCount;
			}
		}

		public void Init (Player.DefaultInventory Inventory) {
			grenadeCount = Inventory.GrenadeCount;
		}

		public void AddGrenades (int amount) {
			grenadeCount += amount;
		}

		public void Throw () {
			grenadeCount -= 1;
		}
	}

	int healthPackCount;
	public int HealthPackCount {
		get {
			return healthPackCount;
		}
	}

}
