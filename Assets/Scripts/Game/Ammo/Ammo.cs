﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : GameItem {
	public enum AmmoType {
		RifleAmmo,
		HandGunAmmo,
		ShotGunShell,
	}

	public AmmoType Type = AmmoType.RifleAmmo;

	public override string GeneralType() {
		return Type.ToString ();
	}
}
