﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour {

	[SerializeField] GameObject itemObject;
	[SerializeField] GameObject listObject;
	[SerializeField] Text creditText;
	[SerializeField] GameObject weaponInfoListObject;
	[SerializeField] GameObject weaponInfoItemObject;

	ScoreKeeper ScoreKeeper;
	Inventory Inventory;

	void Awake() {
		ScoreKeeper = GameManager.Instance.GetComponent<ScoreKeeper> (EPlugInRef.ScoreKeeper);
		Inventory = GameManager.Instance.GetComponent<Inventory> (EPlugInRef.Inventory);

		UpdateUi ();
	}

	void Start() {
		for (int i = 0; i < 3; i++) {
			Ammo.AmmoType type = (Ammo.AmmoType) i;
			GameObject listItem = Instantiate (itemObject, listObject.transform);
			ShopListItem shopListItem = listItem.transform.GetComponent<ShopListItem> ();
			GameItem item = new Ammo ();
			((Ammo) item).Type = type;
			shopListItem.SetUp (item, 20, .5f);
		}

		GameObject grenadeItem = Instantiate (itemObject, listObject.transform);
		ShopListItem grenadeListItem = grenadeItem.transform.GetComponent<ShopListItem> ();
		GameItem grenade = new Granade ();
		grenadeListItem.SetUp (grenade, 1, 10);

	}

	public void OnBuyPressed (GameItem item, int amount, float cost) {
		if (cost > ScoreKeeper.CurrentCredit) {
			Debug.Log ("Not enough money");
			return;
		}

		if (item is Ammo) {
			ScoreKeeper.GiveCredit ((int) cost);
			Inventory.Ammo.AddAmmo (amount, ((Ammo) item).Type);
		}

		if (item is Granade) {
			ScoreKeeper.GiveCredit ((int) cost);
			Inventory.Grenades.AddGrenades (amount);
		}

		UpdateUi ();
	}

	public void UpdateUi () {
		creditText.text = "Your credit: " + ScoreKeeper.CurrentCredit;

		foreach (Transform child in weaponInfoListObject.transform) {
			GameObject.Destroy(child.gameObject);
		}

		Dictionary <Weapon.WeaponType, Weapon> weapons =  Inventory.Weapons.GetWeaponInfo;

		foreach (var weapon in weapons) {
			ShopInfoListItem shopInfoList = Instantiate (weaponInfoItemObject, weaponInfoListObject.transform).GetComponent<ShopInfoListItem> ();
			shopInfoList.SetUp (weapon.Value.ItemName + " (" + weapon.Value.GeneralType () + ")", "Has " + Inventory.Ammo.AmmoCount (weapon.Value.usesAmmo) + " bullets left");
		}

		ShopInfoListItem grenadeInfoListItem =  Instantiate (weaponInfoItemObject, weaponInfoListObject.transform).GetComponent<ShopInfoListItem> ();
		grenadeInfoListItem.SetUp ("HE Grenade", "You have " + Inventory.Grenades.Count);
	}
}
