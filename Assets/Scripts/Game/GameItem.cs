﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameItem : MonoBehaviour {
	public enum GeneralItemType {
		Weapon,
		Ammo,
		Grenade,
		Molotov,
		HealthPack,
	}

	public virtual string GeneralType () {
		return generalType.ToString ();
	} 

	[Header("General")]
	[SerializeField] protected string itemName;
	public string ItemName {
		get {
			return itemName;
		}
	}
	[SerializeField] GeneralItemType generalType;
}
