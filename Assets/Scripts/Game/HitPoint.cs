﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPoint : MonoBehaviour {
	[Header ("Zombie Charecteristics")]
	[Header ("Health")]
	[SerializeField] [Range (50, 200)] float hitPoints;

	public float HitPoints {
		get {
			return hitPoints;
		}
	}

	public float damageTaken;

	public float HitPointsRemaining {
		get {
			return hitPoints - damageTaken;
		}
	}

	public bool IsAlive {
		get {
			return HitPointsRemaining > 0;
		}
	}

	public virtual void Die () {
		if (!IsAlive)
			return;
	}

	public virtual float TakeDamage (float amount) {
		damageTaken += amount;
		return amount;
	}

	public virtual void Reset () {
		damageTaken = 0;
	}
}
