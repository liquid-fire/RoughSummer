﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(GameProgress))]
public class GameProgressCustomInspector : Editor {
	public override void OnInspectorGUI() {
		DrawDefaultInspector ();
		GameProgress gameProgress = (GameProgress) target;

		if (gameProgress.ZombieWave.State == GameProgress.ZombieWaveLogic.WaveState.Ready)
			if(UnityEngine.GUILayout.Button("Next Wave")) {
					gameProgress.ZombieWave.InitiateNextWave ();
			}
	}
}
